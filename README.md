# Hands-on with ROS 2

This workshop has been first held during the
[Virtual RoHOW 2020](https://humanoid.robocup.org/virtual-rohow-2020/)
on Sat Jun 27 [[recording](https://youtu.be/ZgzsYvne5Gs)].
There are no dates for future workshops as of now. You can however
go through the exercises and [get feedback](#report-issues-or-get-help).


![ROS2 Workshop header](ros2-workshop-banner.png)

## Abstract
The next generation of the Robot Operating System (ROS 2) has matured
over the last few years. The central framework and tools have
converged into a stable system, reflected by the fact that by the time
of the Virtual RoHOW 2020 there will have been 2 long term support
releases. More and more important packages are becoming available,
making it ready to be used as a platform for robotics projects. Team
Bold Hearts has been using it effectively to implement their software
framework. In this workshop we aim to present ROS 2, its design and
inner workings and what makes it appealing as a platform for RoboCup
and other robotic applications. The workshop does not expect the
participant to know anything about ROS 1 or 2. The main part of the
workshop consists of several hands-on exercises for participants to
become familiar with ROS 2 and learn how to implement their own
packages. These exercises will involve working with a full 3-D
simulation of a humanoid robot with sensors and actuators, as well as
a computer vision pipeline.

***To get the most out of the hands-on exercises, it is crucial to set up
the docker-based environment at least one day in advance on the 26th of
June (see: [initial setup](#initial-setup)).*** This is to ensure that
we can concentrate on ROS related stuff, rather than downloading
docker images.

## Schedule

* 13:00 (UTC+2) on 27/06/2020
* Presentation: Introduction to ROS 2 (20 min)
* Set-up Humanoid 3-D simulation environment (10 min)
* Hands-on demonstration: ROS 2 basics – topics / services /
  parameters / visualization (30 min)
* Exercise 1: create a sense-act package (60 min)
* Exercise 2: advanced ROS 2 features, e.g., node composition,
  quality-of-service, node lifecycle (60 min)

## Requirements

### Knowledge/experience

* Linux command line basics
* Exercise 1: Python 3
* Exercise 2: C++ / CMake (enough to follow provided steps)
* No knowledge of or experience with ROS 1 or 2 required

### Hardware/software

* Mid-level PC capable of running 3-D physics simulation
* Linux
* Git
* ~3–4 GB free HD space.

**NOTE:** the [initial setup](#setup) outlined in this readme **must**
be prepared and installed by you at least a day before the workshop, i.e.,
not later than the 26/06. It requires a large download of around ~3GB and,
depending on dowload speed, the whole set up can take 10–60min. 

**Note2:** It is possible that there are changes or fixes prior to
the workshop. if you happen to set up the environment further in advance, 
please follow the steps provided [here](#start-and-enter-environment)
on the 26/06, i.e., one day before the workshop, so your environment
will be up-to-date when the workshop starts.

## Setup

We use [ADE](https://ade-cli.readthedocs.io/en/latest/) to create an
easy to set up, consistent development environment. It wraps around
Docker, taking care of a lot of subtleties and providing a useful
command line interface.

### Initial Setup

To set up your development environment you should do the following:

1. Install Docker. For instance for Ubuntu you can follow the
[official
instructions](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
to install `docker-ce`. Note that the official Ubuntu repos contain
`docker.io` which is not recommended.

    Make sure to add yourself to the `docker` group, so you don't have
    to use `sudo` everywhere (change `<your-user>` to be the username on
    your machine and make sure to log out and back in again):

        sudo usermod -aG docker <your-user>

    If everything went well, you should be able to run `docker
    --version` and see a version like `19.03.11`.
2. Create a base directory for your environment. Basically ADE will
   make it seem like you are logging into a different machine where
   this base directory will be your home directory. You can use any
   name, here we will use `wshome`:

       mkdir wshome
       cd wshome
       touch .adehome

   The last command creates an empty file that is a signal to ADE that
   this is going to be the base/home directory.
3. Clone this repository into this new directory:

        git clone https://gitlab.com/boldhearts/ros2_workshop.git

    The important file in this is [.aderc](.aderc), which contains
    information about which Docker image(s) should are to be used, and
    some environment variables that you could change for your specific
    machine.

 
4. Install the ADE command line tools. Follow the [official
instructions](https://ade-cli.readthedocs.io/en/latest/install.html#linux-x86-64-and-aarch64/),
including [updating](https://ade-cli.readthedocs.io/en/latest/install.html#update) the tools after installation.

    After this you should be able to run `ade --version` and see a
    version like `4.2.0`.

### Start and Enter Environment

After the initial setup, you can do the following whenever you want
to start up the environment and do some work, starting from the `wshome`
directory:

1. Update repository

        cd ros2_workshop
        git pull

2. Start up the environment:

        ade start --force --update

    The first time, it may take quite some time because it is
    downloading the base image. You only have to do this once after
    each time you boot up your machine.

3. When this tells you 'ADE has been started', you can enter the
   environment with:

        ade enter

    You should see that the prompt changes to `<your-user>@ade:~$`,
    signifying you are in the environment. If you now run `ls` you
    will see the `ros2_workshop` directory. In general, anything you
    place in the `wshome` directory on your host machine will be
    available in the home of the environment, and any edits either way
    will be reflected on the other side as well; files inside the
    environment are exactly the same as those in `wshome` outside of
    the environment.

    You can enter the environment multiple times in multiple
    terminals (we will do this when running the [dummy robot](#run-ros-2-dummy-robot)).

4. To exit the environment simply run `exit` or press Ctrl+d. The
   environment is still available in the background and you can enter
   again at any time. It doesn't take up much resources in the
   background, but if you still want to stop it, you can run `ade
   stop`.


## Run ROS 2 Dummy Robot

You can now test your enviroment with running the ROS 2 dummy robot. Details are described
in the [official ROS 2 tutorials](https://index.ros.org/doc/ros2/Tutorials/dummy-robot-demo/).
Your development environment comes packed with all necessary packages, and for
running the demo you can skip the `source` commands. I assume you have done the 
steps [1 and 2 from above](#start-and-enter-environment), i.e., your ade is still running
and you entered it. Then type the following into your ade to bring up the dummy robot:

    ros2 launch dummy_robot_bringup dummy_robot_bringup.launch.py

This command starts the dummy robot. To see the robot, you will need to open a second terminal and start another ade session:

    cd wshome/ros2_workshop
    ade enter # <- note that you only enter another instance, ade is already started

In this terminal you can then run `rviz2` to start a robot
visualization. Follow the steps of the
[tutorial](https://index.ros.org/doc/ros2/Tutorials/dummy-robot-demo/)
to learn how to observe the robot and its behavior. Make sure to set a
valid fixed frame, like 'world', as shown in the following animation:

![Dummy robot demo](dummy-robot-demo.gif)

## Run Humanoid Gazebo Simulation

During the workshop we will work with a Gazebo based [simulation of a
full humanoid
robot](https://gitlab.com/boldhearts/ros2_boldbot/-/tree/master)
that is also installed in the environment. As a final test before the
workshop try if it works. While still inside the ADE environment,
launch it with:

    ros2 launch boldbot_sim boldbot_sim_bringup.launch.py

**Note**: the very first run can take some time to start up, as it is
doing initial setup including the syncing models from the open Gazebo
model server. Wait some time until the Gazebo UI has come up. It could
be that the actual robot did not appear due to the startup delay. In
that case, stop the simulation (Ctrl+c in the terminal), and run the
lauch command again.

If this goes well you should see a simulated world like this:

![ROS2 Workshop gazebo](ros2-workshop-gazebo.png)

If that's the case, you are all set up and can look forward to do some
cool things with a humanoid robot in the workshop!

## Report Issues or Get Help

If you have trouble with one of the steps above, please visit the repo's
[issue tracker](https://gitlab.com/boldhearts/ros2_workshop/-/issues) and open an issue. 
We are of course also happy for bug reports or improvements!
