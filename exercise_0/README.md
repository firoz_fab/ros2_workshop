# Exercise 0

This is a first exercise to work with ROS 2. It does not involve any coding, but it will give you an impression of the framework and how it works.

## Requirements

- conducted the initials setup
- see the humanoid robot
- have an extra ade terminal

## Nodes and Topics

ROS 2 breaks the complexity of controlling a robot down into many individual modules -- so called nodes. Nodes use topics to exchange messages. A node can *publish* to and *subscribe* to any number of topics. 

### ROS graph

The Node Graph is a tool to get an overview of the direction of communication. If you run:

	$ rqt_graph
	
you get the currently running and communicating nodes (select “Nodes/Topics (all)” in the top-left dropdown and untick "dead links" and "leaf topics" to see everything).

![The node graph for the running humanoid robot](rosgraph.png)

The above figure shows the output of the Node Graph. The nodes are in elliptical, well, nodes. The rectangular shapes depict the topics the nodes publish or subscribe to. For example, in the bottom right you see that the node `robot_state_publisher` publishes the topic `/robot_description` and subscribes to the topic `/joint_states`.

The node `/boldbot_gazebo` subscribes to the topic `/cm730/joint_commands`. If a node were to publish commands on this topic, the `/boldbot_gazebo` node would execute the commands.
(Note: the naming cm730 is derived from the controller board we use for our physical robot, and it is not ROS specific).

In the next steps we want to interact with the robot, in other words, we want to publish joint commands so that the humanoid robot moves.

### Use ROS 2 CLI to Explore
One powerful tool to interact with the ROS 2 ecosystem is the ROS 2 CLI. Simply type:

	$ ros2

You're then prompted with a number of options to interact with the ROS 2 ecosystem. In the list which is printed when you typed in `ros2` there is already one familiar command: `launch`. This was used to start the simulation and the humanoid robot earlier in the initial setup [LINK].

The CLI prompts help in case you typed in an incomplete command. For example, if you type in `ros2 topic` it will prompt a list of possible commands. One of these commands is `list`, which lists all available topics (Note the switch `-t`, it returns the type of each topic):
	
	$ ros2 topic list -t

The list in our example with a running simulated robot would look similar like this:

	$ ros2 topic list -t
	/camera/camera_info [sensor_msgs/msg/CameraInfo]
	/camera/image_raw [sensor_msgs/msg/Image]
	/clock [rosgraph_msgs/msg/Clock]
	/cm730/joint_commands [mx_joint_controller_msgs/msg/JointCommand]
	/imu/data_raw [sensor_msgs/msg/Imu]
	/joint_states [sensor_msgs/msg/JointState]
	/parameter_events [rcl_interfaces/msg/ParameterEvent]
	/robot_description [std_msgs/msg/String]
	/rosout [rcl_interfaces/msg/Log]
	/tf [tf2_msgs/msg/TFMessage]
	/tf_static [tf2_msgs/msg/TFMessage]


Earlier with`rqt_graph` we got an overview and could study the relationships between nodes and the topics they publish or subscribed to.
With the CLI we can get a few more details, e.g., how to interact with the robot.

For example, we know that the node `/boldbot_gazebo` publishes the `/joint_states` of our humanoid robot, i.e., all current joint positions of the robot.

If you would like to see how that looks, you could echo the messages being published with:

	$ ros2 topic echo /joint_states

If you happen to know the topic of interest, a better way of investigating above is directly requesting the information of this topic. 

	$ ros2 topic  info /joint_states
	Type: sensor_msgs/msg/JointState
	Publisher count: 1
	Subscription count: 1

The output shows the message type of the topic and the number of publishers and subscribers of the topic.


### Use ROS 2 CLI to Move the Robot (i.e., publish a topic)

Let's move the robot. We know there is a topic which awaits joint commands. This topic is `/cm730/joint_commands`.  If we publish to that topic, we can move the robot. Let’s request the info of this topic:

	$ ros2 topic info /cm730/joint_commands
	Type: mx_joint_controller_msgs/msg/JointCommand
	Publisher count: 0
	Subscription count: 1

We can see that there is one subscriber for the topic, but no node which publishes any commands. This is why the robot does not move yet. From above, we see the message type for joint commands is `mx_joint_controller_msgs/msg/JointCommand`. We can use the CLI to publish messages. The next steps show how you can approach publishing to any topic.

Type in the incomplete command:

	$ ros2 topic pub

You get now prompted on how to publish the command. The minimal part of publishing is that you need to name the topic and the message type. All the commands also have code completion, so you don’t have to copy and paste here too much.

So after you typed in `ros2 topic pub `, you just press <Tab> <Tab> and you see the options you can use. Complete your command so it looks like this:

	$ ros2 topic pub --once /cm730/joint_commands mx_joint_controller_msgs/msg/JointCommand

If you press enter, you were to publish an empty message once. The robot won’t move, but it is a valid message. If you <Tab> <Tab> again at the end of the above command, the code completion will show you the message format in multiline yaml format:

	name:\ []\^Jp_gain:\ []\^Ji_gain:\ []\^Jd_gain:\ []\^Jposition:\ []\

Essentially, the message is composed of an array of a name and three arrays of gains and one for the joint positions. A better overview can be gained by studying the message interface directly, type:

	$ ros2 interface show mx_joint_controller_msgs/msg/JointCommand

If we want to let the robot look more to the right right, we can directly manipulate its pan by providing the position in radians, e.g., value “1” and assign this to the name “head-pan”:

	$ ros2 topic pub --once /cm730/joint_commands mx_joint_controller_msgs/msg/JointCommand '{name: ["head-pan"], position: [1]}'

If you want to make the robot look right or straight again, then just change the position accordingly.
